// console.log("Hello World");

// Sum
function printSum(firstAdd, secondAdd){
	let sum = firstAdd + secondAdd; 
	console.log("Displayed sum of " + firstAdd + " and " + secondAdd)
	console.log(sum);
}

printSum(10, 2);

// Difference
function printDifference(firstDiff, secondDiff){
	let difference = firstDiff - secondDiff; 
	console.log("Displayed difference of " + firstDiff + " and " + secondDiff)
	console.log(difference);
}

printDifference(10, 2);

// Product
function returnProduct(firstProduct, secondProduct){
	let multiplication = firstProduct * secondProduct;
	console.log("The product of " + firstProduct + " and " + secondProduct + ":")
	return multiplication;
}

let product = returnProduct(5, 5);
console.log(product);

// Quotient
function returnQuotient(firstQuotient, secondQuotient){
	let division = firstQuotient / secondQuotient;
	console.log("The quotient of " + firstQuotient + " and " + secondQuotient + ":")
	return division;
}

let quotient = returnQuotient(5, 5);
console.log(quotient);

// Circle Area
function returnArea(radius){
	let area = 3.14 * (radius**2);
	console.log ("The result of getting the area of a circle with " + radius + " radius:")
	return area;
}
let circleArea = returnArea(5);
console.log(circleArea);

// Average
function returnAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4)/4;
	console.log ("The average of " + num1 +", "+ num2 +", "+ num3 + ", and " + num4 + ": ")
	return average;
}

let averageVar = returnAverage(3, 4, 5, 6);
console.log(averageVar);

// Percentage
function returnPercentage(score, totalscore){
	let percentage = (score/totalscore)*100;
	console.log("Is "+score+"/"+totalscore+" a passing score?");
	let isPassed = percentage > 75;
	return (isPassed);
}
let isPassingScore = returnPercentage(80, 100);
console.log(isPassingScore);